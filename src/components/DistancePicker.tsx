import React from 'react';

import './DistancePicker.css';
import { config } from '../App-config';

type DistancePickerProps = {
  distance: number;
  setDistance: React.Dispatch<React.SetStateAction<number>>;
};

const DistancePicker = ({
  distance,
  setDistance,
}: DistancePickerProps): JSX.Element => {
  return (
    <>
      <div id="distance-container">
        <span>{distance} km</span>

        <input
          type="range"
          min={config.distance.min}
          max={config.distance.max}
          defaultValue={distance}
          className="slider"
          id="distance-slider"
          onChange={(event) => setDistance(parseInt(event.target.value))}
        />
      </div>
    </>
  );
};

export { DistancePicker };
