import './SpeedPicker.css';
import { config } from '../App-config';

type SpeedPickerProps = {
  speed: number;
  setSpeed: React.Dispatch<React.SetStateAction<number>>;
};

const SpeedPicker = ({ speed, setSpeed }: SpeedPickerProps): JSX.Element => {
  return (
    <>
      <div id="speed-container">
        <span>{speed} km/h</span>

        <input
          type="range"
          min={config.speed.min}
          max={config.speed.max}
          defaultValue={speed}
          className="slider"
          onChange={(event) => setSpeed(parseInt(event.target.value))}
        />
      </div>
    </>
  );
};

export { SpeedPicker };
