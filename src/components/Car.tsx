import { ICar } from '../interfaces/ICar';

import './Car.css';

type CarProps = {
  car: ICar;
  selectedCar: ICar | null;
  setCar: React.Dispatch<React.SetStateAction<ICar | null>>;
};

const Car = ({ car, selectedCar, setCar }: CarProps): JSX.Element => {
  return (
    <>
      <img
        src={`cars/${car.icon}-car.svg`}
        alt={car.name}
        className={`car-icon ${
          selectedCar?.name === car.name ? 'selected' : ''
        }`}
        onClick={() => setCar(car)}
        title={car.name}
      />
    </>
  );
};

export { Car };
