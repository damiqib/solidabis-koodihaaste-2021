import { shallow } from 'enzyme';

import { Car } from './Car';

import { cars } from '../data/cars';

describe('Car component', () => {
  it('should render a selected car', () => {
    const wrapper = shallow(
      <Car car={cars[0]} selectedCar={cars[0]} setCar={jest.fn()} />
    );

    expect(wrapper.find('img.car-icon.selected').exists()).toEqual(true);
  });

  it('should render a not selected car', () => {
    const wrapper = shallow(
      <Car car={cars[0]} selectedCar={cars[1]} setCar={jest.fn()} />
    );

    expect(wrapper.find('img.car-icon').exists()).toEqual(true);
    expect(wrapper.find('img.car-icon.selected').exists()).toEqual(false);
  });

  it('should call setCar when clicked', () => {
    const mockSetCar = jest.fn();

    const wrapper = shallow(
      <Car car={cars[0]} selectedCar={cars[1]} setCar={mockSetCar} />
    );

    expect(mockSetCar).not.toHaveBeenCalled();

    wrapper.find('img').simulate('click');

    expect(mockSetCar).toHaveBeenCalledWith(cars[0]);
  });
});

export {};
