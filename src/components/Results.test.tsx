import { shallow } from 'enzyme';

import { Results } from './Results';

describe('Results component', () => {
  it('should render correct results when driving faster', () => {
    const wrapper = shallow(
      <Results
        speedA={80}
        speedB={100}
        consumptionA={1.2}
        consumptionB={1.5}
        travelTimeA={15}
        travelTimeB={12}
      />
    );

    expect(wrapper.text()).toContain(
      'Normaalinopeudella matka-aika olisi 15.0 minuuttia'
    );
    expect(wrapper.text()).toContain('vertailunopeudella 12.0 minuuttia');
    expect(wrapper.text()).toContain('Ajamalla nopeammin säästäisit');
    expect(wrapper.text()).toContain(
      'Normaalinopeudella polttoaineen kulutus olisi 1.2 litraa'
    );
    expect(wrapper.text()).toContain('vertailunopeudella 1.5 litraa');
    expect(wrapper.text()).toContain('euroa enemmän kuin normaalinopeutta');
  });

  it('should render correct results when driving slower', () => {
    const wrapper = shallow(
      <Results
        speedA={120}
        speedB={100}
        consumptionA={1.7}
        consumptionB={1.5}
        travelTimeA={10}
        travelTimeB={12}
      />
    );

    expect(wrapper.text()).toContain(
      'Normaalinopeudella matka-aika olisi 10.0 minuuttia'
    );
    expect(wrapper.text()).toContain('vertailunopeudella 12.0 minuuttia');
    expect(wrapper.text()).toContain('Ajamalla hitaammin säästäisit');
    expect(wrapper.text()).toContain(
      'Normaalinopeudella polttoaineen kulutus olisi 1.7 litraa'
    );
    expect(wrapper.text()).toContain('vertailunopeudella 1.5 litraa');
    expect(wrapper.text()).toContain('eli säästäisit');
  });
});

export {};
