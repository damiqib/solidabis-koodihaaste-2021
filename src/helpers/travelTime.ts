/**
 * Returns total travel time in minutes
 *
 * @param speed             average speed km/h
 * @param distance          total distance km
 */
const calculateTravelTime = (speed: number, distance: number): number => {
  return (distance / speed) * 60;
};

export { calculateTravelTime };
