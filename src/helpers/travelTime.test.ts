import { calculateTravelTime } from './travelTime';

describe('travel time helper', () => {
  it('should calculate travel time correctly for speed 100 km/h for a distance of 100 km', () => {
    const speed = 100;
    const distance = 100;

    expect(calculateTravelTime(speed, distance)).toEqual(60);
  });

  it('should calculate travel time correctly for speed 50 km/h for a distance of 100 km', () => {
    const speed = 50;
    const distance = 100;

    expect(calculateTravelTime(speed, distance)).toEqual(120);
  });

  it('should calculate travel time correctly for speed 100 km/h for a distance of 200 km', () => {
    const speed = 50;
    const distance = 100;

    expect(calculateTravelTime(speed, distance)).toEqual(120);
  });

  it('should calculate travel time correctly for speed 95 km/h for a distance of 345 km', () => {
    const speed = 95;
    const distance = 345;

    expect(calculateTravelTime(speed, distance)).toEqual(217.89473684210526);
  });
});
