import { calculateConsumption } from './consumption';

describe('consumption helper', () => {
  it('should calculate consumption correctly for car A at speed 1 km/h for a distance of 100 km', () => {
    const speed = 1;
    const baseConsumption = 3;
    const distance = 100;

    expect(calculateConsumption(baseConsumption, speed, distance)).toEqual(3);
  });

  it('should calculate consumption correctly for car B at speed 100 km/h for a distance of 100 km', () => {
    const speed = 100;
    const baseConsumption = 3.5;
    const distance = 100;

    expect(calculateConsumption(baseConsumption, speed, distance)).toEqual(
      8.497545561022946
    );
  });

  it('should calculate consumption correctly for car C at speed 83 km/h for a distance of 162 km', () => {
    const speed = 83;
    const baseConsumption = 4;
    const distance = 162;

    expect(calculateConsumption(baseConsumption, speed, distance)).toEqual(
      13.509853428815925
    );
  });
});
